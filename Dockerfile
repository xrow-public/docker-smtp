# Dockerfile for a Postfix email relay service

FROM fedora:latest

MAINTAINER Björn Dieding <bjoern@xrow.de>

ENV container=docker
ENV term=xterm

RUN dnf -y install postfix;
RUN postconf -e 'mynetworks = 127.0.0.8/32 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8 [::1]'
RUN postconf -e 'mynetworks_style = class'
RUN postconf -e 'inet_interfaces = all'

ADD entrypoint.sh /bin/entrypoint.sh
RUN newaliases && \
    chmod 755 /bin/entrypoint.sh

EXPOSE 25

RUN chmod g=rw /etc/passwd && \
    chgrp -R 0 /var/run/ && \
    chmod -R g=u /var/run/ && \
    chgrp -R 0 /var/log/ && \
    chmod -R g=u /var/log/ && \
    chgrp -R 0 /var/mail/ && \
    chmod -R g=u /var/mail/ && \
    chgrp -R 0 /etc/aliases.db && \
    chmod -R g=u /etc/aliases.db && \
    chgrp -R 0 /etc/postfix/ && \
    chmod -R g=u /etc/postfix/

ENTRYPOINT ["/bin/entrypoint.sh"]

CMD ["postfix", "start-fg"]

USER 1001